
#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>


#define HEAP_SIZE 4096
#define DOUBLED_HEAP_SIZE 8192
#define HALF_HEAP_SIZE 2048
#define MAP_ANONYMOUS    0x20

static struct block_header *get_header(void *contents) {
    return (contents)-offsetof(struct block_header, contents);
}

void test_1() {
    printf("\n--- testing if memory allocation was successful ---\n ");
    struct region *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("test 1 failed: heap was not initialized\n");
        return;
    }

    debug_heap(stdout, heap);

    void *malloc = _malloc(HALF_HEAP_SIZE);

    if (!malloc) {
        printf("test 1 failed: malloc didn't work as expected\n");
        return;
    }

    debug_heap(stdout, heap);
    _free(malloc);
    heap_term();

    printf("test 1 passed\n");
}

void test_2() {
    printf("\n--- testing if one block of several allocated blocks was freed ---\n ");
    struct region *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("test 2 failed: heap was not initialized\n");
        return;
    }

    debug_heap(stdout, heap);

    void *malloc_1 = _malloc(HEAP_SIZE);
    void *malloc_2 = _malloc(HEAP_SIZE);

    if (!malloc_1 || !malloc_2) {
        printf("test 2 failed: malloc didn't work as expected\n");
        return;
    }
    _free(malloc_1);
    if (!malloc_2) {
        printf("test 2 failed: incorrect block status\n");
        return;
    }
    heap_term();

    printf("test 2 passed\n");
}

void test_3() {
    printf("\n--- testing if two blocks of several allocated blocks were freed ---\n ");
    struct region *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("test 3 failed: heap was not initialized\n");
        return;
    }

    debug_heap(stdout, heap);

    void *malloc_1 = _malloc(HEAP_SIZE);
    void *malloc_2 = _malloc(HEAP_SIZE);
    void *malloc_3 = _malloc(HEAP_SIZE);

    if (!malloc_1 || !malloc_2 || !malloc_3) {
        printf("test 3 failed: malloc didn't work as expected\n");
        return;
    }

    _free(malloc_1);
    _free(malloc_2);
    if (!malloc_3) {
        printf("test 3 failed: incorrect block status after allocation\n");
        return;
    }
    heap_term();

    printf("test 3 passed\n");
}


void test_4() {
    printf("\n--- test: ran out of memory, new memory region expands the old one ---\n ");
    struct region *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("test 4 failed: heap was not initialized\n");
        return;
    }

    void *malloc_1 = _malloc(HEAP_SIZE + 1);
    struct block_header *block1 = get_header(malloc_1);

    if (malloc_1 == NULL || block1->capacity.bytes != HEAP_SIZE + 1)
        return;

    void *malloc_2 = _malloc(HALF_HEAP_SIZE);
    struct block_header *block2 = get_header(malloc_2);

    if (malloc_2 == NULL || block2->capacity.bytes != HALF_HEAP_SIZE)
        return;

    _free(malloc_1);
    _free(malloc_2);
    heap_term();

    printf("test 4 passed\n");
}

void test_5() {
    printf("\n--- test: ran out of memory, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated elsewhere ---\n ");
    void *allocated = mmap((void *) HEAP_START, 10, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1,
                           0);
    if (allocated == MAP_FAILED) {
        printf("test 5 failed: unable to pre-allocate memory\n");
        return;
    }

    void *allocated_filled_ptr = _malloc(10);
    if (!allocated_filled_ptr) {
        printf("test 5 failed: _malloc failed to allocate memory\n");
        munmap(allocated, 10);
        return;
    }

    if (allocated == allocated_filled_ptr) {
        printf("test 5 failed: memory regions are unexpectedly the same\n");
    }

    heap_term();
    printf("test 5 passed\n");
}
